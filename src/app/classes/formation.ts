export class Formation {

  constructor(
    public id: number,
    public intitule: string,
    public nomCentreFormation: string,
    public dateDebutFormation: string,
    public dateFinFormation: string,
    public description: string
  ) {}
}
