import { Formation } from './formation';
import { Experience } from './experience';
import { Remarque } from './remarque';
import { Competence } from './competence';
import { MotCle } from './motcle';

export class Personne {

  constructor(
    public id: number,
    public civilite: string,
    public nom: string,
    public prenom: string,
    public poste: string,
    public mail: string,
    public tel: string,
    public adresse: string,
    public codePostal: string,
    public ville: string,
    public dateNaissance: string,
    public linkedIn: string,
    public mobilite: string,
    public permis: string,
    public photo: string,
    public cv: string,
    public centreInteret: string,
    public interne: boolean,
    public formations: Formation[],
    public experiences: Experience[],
    public remarques: Remarque[],
    public competences: Competence[],
    public motscles: MotCle[]
  ) {}
}
