export class Remarque {

  constructor(
    public id: number,
    public remarque: string,
    public date: string
  ) {}
}
