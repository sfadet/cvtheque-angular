export class Experience{

    constructor(
        public id : number,
        public intitule : string,
        public description : string,
        public dateDebutExperience : string,
        public dateFinExperience : string,
        public nomEntreprise : string        
    ) {}
}