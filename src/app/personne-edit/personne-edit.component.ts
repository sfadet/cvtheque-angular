import { Remarque } from './../classes/remarque';
import { Competence } from './../classes/competence';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonneService } from '../services/personne.service';
import { Personne } from '../classes/personne';
import { Subscription } from 'rxjs';
import { DateService } from '../services/date.service';
import { isNull } from 'util';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Experience } from '../classes/experience';
import { Formation } from '../classes/formation';
import { MotCle } from '../classes/motcle';

@Component({
  selector: 'app-personne-edit',
  templateUrl: './personne-edit.component.html',
  styleUrls: ['./personne-edit.component.scss']
})
export class PersonneEditComponent implements OnInit {

  // l'objet personne d'origine
  personneOriginal: Personne;
  // L'objet personne modifié
  personneUpdated: Personne;
  personneEditSubscription: Subscription;
  // Gestion des tableaux jours/mois/années
  tabjours: string[];
  tabmois: string[];
  tabannees: string[];
  // Récupération du jour/mois/année de naissance
  jourNaiss = '01';
  moisNaiss = '01';
  anneeNaiss = '2012';
  // variable de modification d'une expérience
  formUpdateExperience: Experience = new Experience(null, '', '', '', '', '');
  // Variable de modification d'une formation
  indexFE: number;
  formUpdateFormation: Formation = new Formation(null, '', '', '', '', '');
  jourDebutFE = '01';
  moisDebutFE = '01';
  anneeDebutFE = '2019';
  jourFinFE = '01';
  moisFinFE = '01';
  anneeFinFE = '2019';
  // Variables d'ajout d'une formation et d'une expérience
  formAddFormation: Formation = new Formation(null, '', '', '', '', '');
  formAddExperience: Experience = new Experience(null, '', '', '', '', '');
  jourDebutAddFE = '01';
  moisDebutAddFE = '01';
  anneeDebutAddFE = '2019';
  jourFinAddFE = '01';
  moisFinAddFE = '01';
  anneeFinAddFE = '2019';
  // Variables d'ajout de mot-cle et de competence
  addMotCle = '';
  addComp = '';
  // Varialble d'ajout de remarque
  addRemarque: Remarque = new Remarque(null, '' , '');
  // Variables pour l'envoie des fichiers photo et cv
  formImg: FormGroup;
  error: string;
  tempImgFileName = '';
  tempCvFileName = '';

  // Import dans le constructeur des services nécessaires au composant
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pServ: PersonneService,
    private dateServ: DateService,
    private formBuilder: FormBuilder,
    private httpClient: HttpClient) { }

  // Initialisation du composant
  ngOnInit() {
    this.personneOriginal = this.resetPersonne();
    this.personneUpdated = this.resetPersonne();
    this.personneEditSubscription = this.pServ.personneEditSubject.subscribe(
      (personne: Personne) => {
        this.personneOriginal = personne;
        this.personneUpdated = personne;
        if (!isNull(this.personneUpdated.dateNaissance)) {
          const tabSplitDate = this.personneUpdated.dateNaissance.split('/');
          this.jourNaiss = tabSplitDate[0];
          this.moisNaiss = tabSplitDate[1];
          this.anneeNaiss = tabSplitDate[2];
        }
      }
    );
    // Remplissage des variables de date
    this.tabjours = this.dateServ.tabJours;
    this.tabmois = this.dateServ.tabMois;
    this.tabannees = this.dateServ.tabAnnees;

    // vérification que l'id est bien un nombre
    if (isNaN(this.route.snapshot.params['id'])) {
      // sinon redirection vers la liste des personnes
      this.router.navigate(['personne/list']);
    } else {
      // si oui, récupération de l'id
      this.pServ.getPersonneById(this.route.snapshot.params['id']);
    }

    this.formImg = this.formBuilder.group({
      photo: ['']
    });
  }

  // Action effectuées lors du click sur le bouton de mise à jour
  // p -> la personne à mettre à jour
  // stay -> un booleen pour savoir s'il faut rester sur la page ou etre redirigé vers la liste des personnes
  onUpdatePersonne(p: Personne, stay: boolean) {
    p.dateNaissance = this.jourNaiss + '/' + this.moisNaiss + '/' + this.anneeNaiss;
    this.pServ.updatePersonne(p);
    if (!stay) {
      this.router.navigate(['personne/list']);
    }
  }

  // Permet de retourner une personne vide, pour ne pas avoir des problèmes (undefined)
  resetPersonne() {
    return new Personne(null, 'Mr', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', false, [], [], [], [], []);
  }
  // Permet de faire un reset pour les dates de modifications des Formations et des Experiences
  resetDatesUpdateFormExp() {
    this.jourDebutFE = '01';
    this.moisDebutFE = '01';
    this.anneeDebutFE = '2019';
    this.jourFinFE = '01';
    this.moisFinFE = '01';
    this.anneeFinFE = '2019';
  }

  //////////////////////////////////////////////////////
  // Ajout / Suppression Mots-clés et compétences
  //////////////////////////////////////////////////////
  onAjoutMotCle() {
    if (this.addMotCle !== '') {
      const mc = new MotCle(null, this.addMotCle);
      this.personneUpdated.motscles.push(mc);
      this.addMotCle = '';
    }
  }
  onAjoutCompetence() {
    if (this.addComp !== '') {
      const c = new Competence(null, this.addComp);
      this.personneUpdated.competences.push(c);
      this.addComp = '';
    }
  }
  onDeleteMotCle(index: number) {
    this.personneUpdated.motscles.splice(index, 1);
  }
  onDeleteCompetence(index: number) {
    this.personneUpdated.competences.splice(index, 1);
  }
  //////////////////////////////////////////////////////
  // Modifications des Formations et Expériences
  //////////////////////////////////////////////////////
  // Selection d'une expérience à modifer
  onSelectUpdateExperience(i: number) {
    this.indexFE = i;
    this.formUpdateExperience = new Experience(null,
      this.personneUpdated.experiences[i].intitule,
      this.personneUpdated.experiences[i].nomEntreprise,
      '',
      '',
      this.personneUpdated.experiences[i].description);
    // Gestion des dates
    if (this.formUpdateExperience.dateDebutExperience !== null) {
      if (this.formUpdateExperience.dateDebutExperience !== '') {
        const tabSplitDateD = this.formUpdateExperience.dateDebutExperience.split('/');
        tabSplitDateD[0] !== '' ? this.jourDebutFE = tabSplitDateD[0] : this.jourDebutFE = '01';
        tabSplitDateD[1] !== '' ? this.moisDebutFE = tabSplitDateD[1] : this.moisDebutFE = '01';
        tabSplitDateD[2] !== '' ? this.anneeDebutFE = tabSplitDateD[2] : this.anneeDebutFE = '2019';
      } else {
        this.jourDebutFE = '01';
        this.moisDebutFE = '01';
        this.anneeDebutFE = '2019';
      }
    }
    if (this.formUpdateExperience.dateFinExperience != null) {
      if (this.formUpdateExperience.dateFinExperience !== '') {
        const tabSplitDateD = this.formUpdateExperience.dateFinExperience.split('/');
        tabSplitDateD[0] !== '' ? this.jourFinFE = tabSplitDateD[0] : this.jourFinFE = '01';
        tabSplitDateD[1] !== '' ? this.moisFinFE = tabSplitDateD[1] : this.moisFinFE = '01';
        tabSplitDateD[2] !== '' ? this.anneeFinFE = tabSplitDateD[2] : this.anneeFinFE = '2019';
      } else {
        this.jourFinFE = '01';
        this.moisFinFE = '01';
        this.anneeFinFE = '2019';
      }
    }
  }
  // modification d'une expérience
  onUpdateExperience(i) {
    this.personneOriginal.experiences[i] = new Experience(
      this.formUpdateExperience.id,
      this.formUpdateExperience.intitule,
      this.formUpdateExperience.nomEntreprise,
      this.jourDebutFE + '/' + this.moisDebutFE + '/' + this.anneeDebutFE,
      this.jourFinFE + '/' + this.moisFinFE + '/' + this.anneeFinFE,
      this.formUpdateExperience.description
    );
  }
  // Selection d'une formation à modifier
  onSelectUpdateFormation(i: number) {
    this.indexFE = i;
    this.formUpdateFormation = new Formation(null,
      this.personneUpdated.formations[i].intitule,
      this.personneUpdated.formations[i].nomCentreFormation,
      '',
      '',
      this.personneUpdated.formations[i].description);
    // Gestion des dates
    if (this.formUpdateFormation.dateDebutFormation !== null) {
      if (this.formUpdateFormation.dateDebutFormation !== '') {
        const tabSplitDateD = this.formUpdateFormation.dateDebutFormation.split('/');
        tabSplitDateD[0] !== '' ? this.jourDebutFE = tabSplitDateD[0] : this.jourDebutFE = '01';
        tabSplitDateD[1] !== '' ? this.moisDebutFE = tabSplitDateD[1] : this.moisDebutFE = '01';
        tabSplitDateD[2] !== '' ? this.anneeDebutFE = tabSplitDateD[2] : this.anneeDebutFE = '2019';
      } else {
        this.jourDebutFE = '01';
        this.moisDebutFE = '01';
        this.anneeDebutFE = '2019';
      }
    }
    if (this.formUpdateFormation.dateFinFormation != null) {
      if (this.formUpdateFormation.dateFinFormation !== '') {
        const tabSplitDateD = this.formUpdateFormation.dateFinFormation.split('/');
        tabSplitDateD[0] !== '' ? this.jourFinFE = tabSplitDateD[0] : this.jourFinFE = '01';
        tabSplitDateD[1] !== '' ? this.moisFinFE = tabSplitDateD[1] : this.moisFinFE = '01';
        tabSplitDateD[2] !== '' ? this.anneeFinFE = tabSplitDateD[2] : this.anneeFinFE = '2019';
      } else {
        this.jourFinFE = '01';
        this.moisFinFE = '01';
        this.anneeFinFE = '2019';
      }
    }
  }
  // modification d'une formation
  onUpdateFormation(i) {
    this.personneOriginal.formations[i] = new Formation(
      this.formUpdateFormation.id,
      this.formUpdateFormation.intitule,
      this.formUpdateFormation.nomCentreFormation,
      this.jourDebutFE + '/' + this.moisDebutFE + '/' + this.anneeDebutFE,
      this.jourFinFE + '/' + this.moisFinFE + '/' + this.anneeFinFE,
      this.formUpdateFormation.description
    );
  }

  // Suppression d'une expérience
  onDeleteExperience(index: number) {
    this.personneUpdated.experiences.splice(index, 1);
  }
  // Suppression d'une formation
  onDeleteFormation(index: number) {
    this.personneUpdated.formations.splice(index, 1);
  }
  // Suppression d'une remarque
  onDeleteRemarque(index: number) {
    this.personneUpdated.remarques.splice(index, 1);
  }
  // Suppression de la photo
  onDeletePhoto() {
    this.personneUpdated.photo = '';
  }
  // Suppression du CV
  onDeleteCv() {
    this.personneUpdated.cv = '';
  }
  // Ajout d'une formation
  onAddFormation() {
    this.personneUpdated.formations.push(
      new Formation(null,
        this.formAddFormation.intitule,
        this.formAddFormation.nomCentreFormation,
        this.jourDebutAddFE + '/' + this.moisDebutAddFE + '/' + this.anneeDebutAddFE,
        this.jourFinAddFE + '/' + this.moisFinAddFE + '/' + this.anneeFinAddFE,
        this.formAddFormation.description));
    this.formAddFormation = new Formation(null, '', '', '', '', '');
    this.jourDebutAddFE = '01';
    this.moisDebutAddFE = '01';
    this.anneeDebutAddFE = '2019';
    this.jourFinAddFE = '01';
    this.moisFinAddFE = '01';
    this.anneeFinAddFE = '2019';
  }
  // Ajout d'une Experience
  onAddExperience() {
    this.personneUpdated.experiences.push(
      new Experience(null,
        this.formAddExperience.intitule,
        this.formAddExperience.description,
        this.jourDebutAddFE + '/' + this.moisDebutAddFE + '/' + this.anneeDebutAddFE,
        this.jourFinAddFE + '/' + this.moisFinAddFE + '/' + this.anneeFinAddFE,
        this.formAddExperience.nomEntreprise));
    this.formAddExperience = new Experience(null, '', '', '', '', '');
    this.jourDebutAddFE = '01';
    this.moisDebutAddFE = '01';
    this.anneeDebutAddFE = '2019';
    this.jourFinAddFE = '01';
    this.moisFinAddFE = '01';
    this.anneeFinAddFE = '2019';
  }
  // Ajout d'une remarque
  onAddRemarque() {
    const date = new Date(Date.now());
    const day = ('0' + date.getDate()).slice(-2);
    const monthIndex = ('0' + (date.getMonth() + 1)).slice(-2)
    const year = date.getFullYear();
    this.personneUpdated.remarques.push(new Remarque(null, this.addRemarque.remarque, day + '/' + monthIndex + '/' + year));
    this.addRemarque.remarque = '';
  }

  //////////////////////////////////////////////////////
  // Envoie de fichiers
  //////////////////////////////////////////////////////
  // Fonction qui surveille la modification des inputs d'envoie de fichier
  onFileChangeImg(event, type) {
    if (event.target.files.length > 0) {
      // renommage du fichier image pour eviter les doublons
      const tabfile: string[] = event.target.files[0].name.split('.');
      if (type === 'img') {
        this.tempImgFileName = this.personneUpdated.nom + '-' + this.personneUpdated.prenom + '.' + tabfile[tabfile.length - 1];
        console.log(this.tempImgFileName);
      }
      if (type === 'cv') {
        this.tempCvFileName = this.personneUpdated.nom + '-' + this.personneUpdated.prenom + '.' + tabfile[tabfile.length - 1];
        console.log(this.tempCvFileName);
      }
      // récupération du fichier
      const file = event.target.files[0];
      this.formImg.get('photo').setValue(file);
    }
  }
  // Fonction qui permet l'envoie des images et des cvs
  onSubmitImg(type: string) {
    const formData = new FormData();
    if (type === 'img') {
      formData.append('nomfichier', this.tempImgFileName);
    }
    if (type === 'cv') {
      formData.append('nomfichier', this.tempCvFileName);
    }
    formData.append('file', this.formImg.get('photo').value);

    this.httpClient.post<any>('http://localhost:8080/upload/' + type, formData).subscribe(
      (res) => console.log("coucou"),
      (err) => {
        if (type === 'img') {
          this.personneUpdated.photo = this.tempImgFileName;
        }
        if (type === 'cv') {
          this.personneUpdated.cv = this.tempCvFileName;
        }
      }
    );
  }
}
