import { Personne } from './../classes/personne';
import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../services/personne.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-personne-list',
  templateUrl: './personne-list.component.html',
  styleUrls: ['./personne-list.component.scss']
})
export class PersonneListComponent implements OnInit {

  personnes: Personne[] = [];
  personnesFilter: Personne[] = [];
  personneSubscription: Subscription;
  // Personne à supprimer
  personneDeleteNom = '';
  personneDeleteId: number = null;
  // Critères de filtre
  searchByNom = '';       // filtre sur le nom
  searchByPoste = '';     // filtre sur le nom

  constructor(private pServ: PersonneService) { }

  ngOnInit() {
    this.personneSubscription = this.pServ.personneSubject.subscribe(
      (personnes: Personne[]) => {
        this.personnes = personnes;
        this.personnesFilter = personnes;
      }
    );
    this.pServ.getPersonnes();
  }

  // Permet de changer la liste en fonction des champs de recherche
  // Le tableau avec les filtres récupère le tableau entier à chaque fois
  // ensuite il est modifié en fonction des champs de recherches qui sont saisis
  searchFilter() {
    this.personnesFilter = this.personnes;
    // filtre 1 : recherche sur le nom : searchByNom
    // utilisé pour le nom et le prénom
    if (this.searchByNom !== '') {
      this.personnesFilter = this.personnesFilter.filter(res => {
        return res.nom.toLocaleLowerCase().match(this.searchByNom.toLocaleLowerCase());
      });
      // this.personnesFilter = this.personnesFilter.filter(res => {
      //   return res.prenom.toLocaleLowerCase().match(this.searchByNom.toLowerCase());
      // });
    }
    // filtre 2 : recherche sur le poste // TODO
    if (this.searchByPoste !== '') {
      this.personnesFilter = this.personnesFilter.filter(res => {
        return res.poste.toLocaleLowerCase().match(this.searchByPoste.toLocaleLowerCase());
      });
    }
  }

  // Selection de la personne à supprimer
  onSelectDeletePersonne(nom: string, id: number) {
    this.personneDeleteNom = nom;
    this.personneDeleteId = id;
    console.log(nom + ' ' + id);
  }
  // Confirmation de la suppression
  onDeletePersonne(id: number) {
    this.pServ.deletePersonne(id);
  }

}
