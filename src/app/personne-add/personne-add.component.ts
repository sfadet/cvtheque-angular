import { Personne } from './../classes/personne';
import { PersonneService } from './../services/personne.service';
import { Component, OnInit } from '@angular/core';
import { DateService } from '../services/date.service';

@Component({
  selector: 'app-personne-add',
  templateUrl: './personne-add.component.html',
  styleUrls: ['./personne-add.component.scss']
})
export class PersonneAddComponent implements OnInit {


  // variable qui récupère les données du formulaire
  formAddPersonne: Personne;
  // Gestion des tableaux jours/mois/années
  tabjours: string[];
  tabmois: string[];
  tabannees: string[];
  // Récupération du jour/mois/année de naissance
  jourNaiss = '01';
  moisNaiss = '01';
  anneeNaiss = '2012';
  // Activation du bouton d'ajout
  activAddButton = true;

  constructor(
    private pServ: PersonneService,
    private dateServ: DateService) { }

  ngOnInit() {
    this.formAddPersonne = this.resetPersonne();
    this.tabjours = this.dateServ.tabJours;
    this.tabmois = this.dateServ.tabMois;
    this.tabannees = this.dateServ.tabAnnees;
    this.pServ.getPersonnes();
  }

  onAddPersonne() {
    this.formAddPersonne.dateNaissance = this.jourNaiss + '/' + this.moisNaiss + '/' + this.anneeNaiss;
    this.pServ.addPersonne(this.formAddPersonne);
    this.formAddPersonne = this.resetPersonne();
  }

  resetPersonne() {
    return new Personne(null, 'Mr', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', false, [], [], [], [], []);
  }

  checkValidForm() {
    if (this.formAddPersonne.nom === '' || this.formAddPersonne.prenom === '' || this.formAddPersonne.poste === '') {
      this.activAddButton = true;
    } else {
      this.activAddButton = false;
    }
    console.log('ok');
  }
}
