import { Injectable } from '@angular/core';
import { Personne } from '../classes/personne';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  // La liste des personnes
  personnes: Personne[] = [];
  personneSubject = new Subject<Personne[]>();
  // La personne éditée
  personneEdit: Personne;
  personneEditSubject = new Subject<Personne>();
  // l'url de base
  private baseUrl = 'http://localhost:8080/cvtheque/personne';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _httpClient: HttpClient
  ) {
    this.getPersonnes();
  }

  emitPersonnes() {
    this.personneSubject.next(this.personnes);
  }
  emitPersonneEdit() {
    this.personneEditSubject.next(this.personneEdit);
  }

  // Récupérer les personnes
  getPersonnes() {
    this._httpClient
      .get<Personne[]>(this.baseUrl)
      .subscribe(
        (response) => {
          this.personnes = response;
          this.emitPersonnes();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  // récupération d'une personne par son id
  getPersonneById(id: number) {
    this._httpClient
      .get<Personne>(this.baseUrl + '/' + id)
      .subscribe(
        (response) => {
          this.personneEdit = response;
          this.emitPersonneEdit();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  // Fonctions CRUD
  ///////////////////////////////////////////////////////////////
  // Ajout d'une persoone ///////////////////////////////////////
  addPersonne(personne: Personne) {
    // TODO à tester
    this._httpClient
      .post<Personne>(this.baseUrl, personne)
      .subscribe(
        (response) => {
          this.router.navigate(['personne/edit/' + response.id]);
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  // Modification d'une persoone ////////////////////////////////
  updatePersonne(personne: Personne) {
    // TODO à tester
    this._httpClient
      .put(this.baseUrl, personne)
      .subscribe(
        () => {
          this.getPersonnes();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
  // Suppression d'une persoone /////////////////////////////////
  deletePersonne(id: number) {
    // TODO à tester
    this._httpClient
      .delete(this.baseUrl + '/' + id)
      .subscribe(
        () => {
          this.getPersonnes();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }
}
