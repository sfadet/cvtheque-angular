import { PersonneAddComponent } from './personne-add/personne-add.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PersonneListComponent } from './personne-list/personne-list.component';
import { PersonneEditComponent } from './personne-edit/personne-edit.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

const appRoutes: Routes = [
  { path: 'personne/edit/:id', component: PersonneEditComponent},
  { path: 'personne/add', component: PersonneAddComponent},
  { path: 'personne/list', component: PersonneListComponent},
  { path: '', component: PersonneListComponent},
  { path: '**', component: PersonneListComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    PersonneAddComponent,
    PersonneListComponent,
    PersonneEditComponent,
    FileUploadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
